# -*- coding: utf-8 -*-
import copy
import json
import html
import time
import scrapy
from scrapy.shell import inspect_response
from ..scrapy_selenium import SeleniumRequest
from scrapy.exceptions import CloseSpider, DontCloseSpider


class SpecsSpider(scrapy.Spider):
    name = 'specs'
    allowed_domains = ['dell.com']
    start_urls = [
        'https://www.dell.com/en-us/work/shop/servers-storage-and-networking/sf/poweredge-tower-servers',
        'https://www.dell.com/en-us/work/shop/servers-storage-and-networking/sf/poweredge-rack-servers',
        'https://www.dell.com/en-us/work/shop/servers-storage-and-networking/sf/rack-infrastructure',
        'https://www.dell.com/en-us/work/shop/servers-storage-and-networking/sf/modular-infrastructure',
    ]

    items = []

    api_url = 'https://www.dell.com/csbapi/en-us/productanavfilter/GetSystemsResults?ProductCode={product_id}&page=1&pageSize=50&sappOrderCode=&preview='
    api_headers = {
        'accept-language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7',
        'accept': 'application/json, text/plain, */*',
        'authority': 'www.dell.com',
        'dnt': '1',
        'host': 'www.dell.com',
        'referer': None,
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36',
    }


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider


    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, dont_filter=True)


    def parse(self, response):
        # inspect_response(response, self)
        urls_css = '.franchise-series-section .franchise-series-title a ::attr(href)'
        urls = response.css(urls_css).extract()

        for url in urls:
            yield response.follow(url, callback=self.parse_item)


    def parse_item(self, response):
        # inspect_response(response, self)

        d = {}

        d['url'] = response.url
        d['product_id'] = response.css('meta[name="ProductId"] ::attr(content)').extract_first()
        d['category'] = response.css('meta[name="CategoryId"] ::attr(content)').extract_first()
        d['specs'] = dict(get_specs(response))

        urls_css = '//*[@id="hero-container"]//a[contains(text(), "View configurations")]/@href'
        urls = [response.urljoin(url) for url in response.xpath(urls_css).extract()]

        if not urls:
            self.logger.debug('Unable to find configurations in <{}>'.format(response.url))
            d['configurations'] = {}
            return d

        for url in urls:
            t = (url, d)
            self.items.append(t)


    def spider_idle(self, spider):
        if self.items:
            url, item = self.items.pop(0)
            spider.crawler.engine.crawl(SeleniumRequest(url=url, meta={'item': item}, callback=self.get_configurations), spider)
            raise DontCloseSpider


    def get_configurations(self, response):
        # inspect_response(response, self)

        driver = response.meta['driver']
        item = response.meta['item']

        url = self.api_url.format(product_id=item['product_id'])
        headers = self.api_headers
        headers['referer'] = response.url
        cookies = driver.get_cookies()

        return scrapy.Request(url, headers=headers, cookies=cookies, meta={'item': item}, callback=self.parse_configurations)


    def parse_configurations(self, response):
        # inspect_response(response, self)

        item = response.meta['item']

        try:
            data = json.loads(response.text)
            item['configurations'] = data['Results']
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(data)

        return item


def get_specs(response):
    for section in response.css('#techspecs .povw-flexbox>.povw-spec'):
        section_name = section.css('h4 strong ::text').extract_first()
        if section_name:
            section_name = html.unescape(section_name).strip()
        else:
            section_name = 'NA'

        text = list(filter(None, [html.unescape(t).strip() for t in section.css('.povw-spec>p ::text').extract()]))

        yield section_name, text
