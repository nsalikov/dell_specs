# -*- coding: utf-8 -*-
import json
import html
import time
import scrapy
from http.cookies import SimpleCookie
from scrapy.shell import inspect_response
from ..scrapy_selenium import SeleniumRequest
from scrapy.exceptions import CloseSpider, DontCloseSpider
# from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class SpecsSpider(scrapy.Spider):
    name = 'test'
    allowed_domains = ['dell.com']
    start_urls = [
        'https://www.dell.com/en-us/work/shop/productdetailstxn/poweredge-t140',
    ]

    api_url = 'https://www.dell.com/csbapi/en-us/productanavfilter/GetSystemsResults?ProductCode={product_id}&page=1&pageSize=50&sappOrderCode=&preview='
    api_headers = {
        'accept-language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7',
        'accept': 'application/json, text/plain, */*',
        'authority': 'www.dell.com',
        'dnt': '1',
        'host': 'www.dell.com',
        'referer': None,
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36',
    }



    def start_requests(self):
        for url in self.start_urls:
            #yield scrapy.Request(url, callback=self.parse, dont_filter=True)
            item = {'product_id': 'poweredge-t140'}
            yield SeleniumRequest(url=url, meta={'item': item}, callback=self.get_configurations)


    def get_configurations(self, response):
        # inspect_response(response, self)

        driver = response.meta['driver']
        item = response.meta['item']

        # driver.find_element_by_xpath('//a[contains(@data-metrics, "nextbtn")]').click()
        # time.sleep(2)

        url = self.api_url.format(product_id=item['product_id'])
        headers = self.api_headers
        headers['referer'] = response.url
        cookies = driver.get_cookies()

        return scrapy.Request(url, headers=headers, cookies=cookies, meta={'item': item}, callback=self.parse_configurations)


    def parse_configurations(self, response):
        # inspect_response(response, self)

        item = response.meta['item']

        try:
            data = json.loads(response.text)
            item['configurations'] = data['Results']
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(data)

        return item


def get_specs(response):
    for section in response.css('#techspecs .povw-flexbox>.povw-spec'):
        section_name = section.css('h4 strong ::text').extract_first()
        if section_name:
            section_name = html.unescape(section_name).strip()
        else:
            section_name = 'NA'

        text = list(filter(None, [html.unescape(t).strip() for t in section.css('.povw-spec>p ::text').extract()]))

        yield section_name, text


def string2cookies(s):
    cookie = SimpleCookie()
    cookie.load(s)

    cookies = {}
    for key, morsel in cookie.items():
        cookies[key] = morsel.value

    return cookies
